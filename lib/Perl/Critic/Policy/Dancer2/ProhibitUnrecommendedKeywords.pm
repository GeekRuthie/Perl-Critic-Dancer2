package Perl::Critic::Policy::Dancer2::ProhibitUnrecommendedKeywords;

# VERSION
# AUTHORITY
# ABSTRACT: Trigger perlcritic alerts on unrecommended Dancer2 keywords
use 5.006001;
use strict;
use warnings;
use Readonly;

use Perl::Critic::Utils qw{
    :booleans :characters :severities :classification :data_conversion
};
use base 'Perl::Critic::Policy';

sub default_severity { return $SEVERITY_LOW }
sub default_themes   { return qw( dancer2 ) }
sub applies_to       { return 'PPI::Token::Word' }

Readonly::Hash my %unrecommended_words =>
    ( { params => q{body_parameters', 'route_parameters', or 'query_parameters} }, );
Readonly::Scalar my $EXPL =>
    'You are using a Dancer2 keyword that is not recommended, and may be deprecated in the future.';

sub violates {
   my ( $self, $elem, $doc ) = @_;
   my $included = $doc->find_any(
      sub {
         $_[1]->isa('PPI::Statement::Include')
             and defined( $_[1]->module() )
             and ( $_[1]->module() eq 'Dancer2' )
             and $_[1]->type() eq 'use';
      }
   );
   return if !$included;
   if ( defined $unrecommended_words{$elem} ) {
      return if is_hash_key($elem);
      my $alternative = $unrecommended_words{$elem};
      my $desc        = qq{Use '$alternative' instead of unrecommended Dancer2 keyword '$elem'};
      return $self->violation( $desc, $EXPL, $elem );
   }
   return;
}

1;

__END__

=pod

=head1 AFFILIATION
 
This policy is part of L<Perl::Critic::Dancer2>.

=head1 CONFIGURATION
 
This Policy is not configurable except for the standard options.

=head1 DESCRIPTION

The L<Dancer2> team has a deprecation policy, detailed at
L<Dancer2::DeprecationPolicy>, that will, in time, cause certain
keywords to be removed from the Dancer2 codebase. The keywords
addressed in this policy have newer substitutes and/or have been
suggested for deprecation.

=cut
